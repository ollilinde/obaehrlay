import { useEffect, useState } from "react"

import "./SplashScreen.scss";

const SplashScreenComp = () => {
  return (
    <div className="Splash">
      <header className="Splash-header">
        <h1>O<strong>baehr</strong>lay</h1>
        <p><code>Stream Overlays for Techies.</code></p>
      </header>
    </div>
  )
}

export const SplashScreen = ({ children }: { children: any}) => {

  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    const timeout = setTimeout(() => {
      setIsLoading(false)
    }, 3000)

    return () => clearTimeout(timeout)
  }, [])

  return (
    <>
      {isLoading ? <SplashScreenComp /> : children}
    </>
  )

}