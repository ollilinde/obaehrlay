import Box from "@mui/material/Box"
import Card from "@mui/material/Card"

export const Index = () => {
  return (
    <div>
      <h2>Welcome!</h2>
      <p>
        Obaehrlay is a free and powerful tool to funnel your interactions into your stream. It's developer focussed,
        so if you have experience developing for the web you should be able to develop great overlays. You're not
        bound to any online service provided from Obaehrlay. You can (and should) connect to integrated services like
        Twitch, Spotify or even Streamer.bot to unlock the full potential. 
      </p>

      <Box sx={{ display: 'grid', gap: 2, gridTemplateColumns: '1fr 1fr' }}>
        <Card sx={{ p: 2, display: 'grid', gridTemplateRows: '1fr 1fr', justifyItems: 'center', alignItems: 'center' }}>
          <img src="/streamerbot.svg" style={{ width: '50%', maxWidth: 400, display: 'block', margin: '0 auto' }} />
          I want to integrate with Streamer.bot
        </Card>
        <Card sx={{ p: 2, display: 'grid', gridTemplateRows: '1fr 1fr', justifyItems: 'center', alignItems: 'center' }}>
          <img src="/twitch.svg" style={{ width: '50%', maxWidth: 400, display: 'block', margin: '0 auto' }} />
          I want to integrate directly with Twitch.tv
        </Card>
      </Box>
    </div>
  )
}